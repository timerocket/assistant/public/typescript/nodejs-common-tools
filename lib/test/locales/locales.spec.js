"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const locales_1 = require("../../locales/locales");
describe("locales", () => {
    it("should configure locales", () => {
        expect(locales_1.i18nData.__({
            phrase: "i-am-healthy",
            locale: "eng-US",
        })).toBe("I'm healthy");
    });
});
