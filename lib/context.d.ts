import { ClientInterface, LocaleInterface, MessageContextInterface } from "@timerocket/data-model";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "./config";
export declare class Context {
    correlationId: string;
    config: DefaultConfig;
    client: ClientInterface;
    readonly locale: LocaleInterface;
    readonly messageContext: MessageContextInterface;
    readonly logger: LoggerService;
    started: Date;
    constructor(correlationId: string, logger: LoggerService, config: DefaultConfig, client: ClientInterface, locale: LocaleInterface, messageContext: MessageContextInterface, started?: Date);
}
